"Resource/UI/HudPlayerArmor.res"
{
	"PlayerStatusArmorValue"
	{
		"ControlName"	"CTFLabel"
		"fieldName"		"PlayerStatusArmorValue"
		"xpos"			"100"
		"xpos_minmode"	"75"
		"ypos"			"52"	[$WIN32]
		"ypos"			"55"	[$X360]
		"zpos"			"5"
		"wide"			"50"
		"tall"			"18"
		"visible"		"1"
		"enabled"		"1"
		"labelText"		"%Armor%"
		"textAlignment"	"center"	
		"font"			"HudClassArmor"
		"fgcolor"		"TanDark"
	}
}