WeaponData
{
	// Attributes Base.
	"printname"		"#TF_Weapon_Revolver"
	"BuiltRightHanded"	"0"
	"weight"		"3"
	"WeaponType"		"secondary"
	"ITEM_FLAG_NOITEMPICKUP" 	"1"
		
	// Attributes TF.
	"Damage"			"10"
	"Range"				"4096"
	"BulletsPerShot"	"1"
	"Spread"			"0.0"
	"TimeFireDelay"		"1"
	"ProjectileType"	"projectile_dart"
	"DoInstantEjectBrass"	"0"
	"HasTeamSkins_Viewmodel"			"1"
	"TracerEffect"		"projectile_syringe"
	"MaxAmmo"		"40"


	// Ammo & Clip.
	"primary_ammo"		"TF_AMMO_SHELLS"
	"secondary_ammo"	"None"

	// Buckets.
	"bucket"		"1"
	"bucket_position"	"0"

	// Animation.
	"viewmodel"		"models/weapons/v_models/v_revolver_spy.mdl"
	"playermodel"		"models/weapons/w_models/w_revolver.mdl"
	"anim_prefix"		"ac"


	// Muzzleflash
	//"MuzzleFlashModel"	"models/effects/muzzleflash/minigunmuzzle.mdl"
	//"MuzzleFlashParticleEffect" "muzzle_revolver"
	//"MuzzleflashModelScale"	".3"

	// Sounds.
	// Max of 16 per category (ie. max 16 "single_shot" sounds).
	SoundData
	{
		"reload"		"Weapon_Revolver.WorldReload"
		"empty"			"Weapon_Revolver.ClipEmpty"	
		"single_shot"		"Weapon_Dartgun.Single"
		"burst"			"Weapon_Revolver.SingleCrit"
	}

	// Weapon Sprite data is loaded by the Client DLL.
	TextureData
	{
		"weapon"
		{
				"file"		"sprites/bucket_revolver"
				"x"		"0"
				"y"		"0"
				"width"		"200"
				"height"		"128"
		}
		"weapon_s"
		{	
				"file"		"sprites/bucket_revolver"
				"x"		"0"
				"y"		"0"
				"width"		"200"
				"height"		"128"
		}
		"ammo"
		{
				"file"		"sprites/a_icons1"
				"x"			"55"
				"y"			"60"
				"width"		"73"
				"height"	"15"
		}
		"crosshair"
		{
				"file"		"sprites/crosshairs"
				"x"			"64"
				"y"			"0"
				"width"		"32"
				"height"	"32"
		}
		"autoaim"
		{
				"file"		"sprites/crosshairs"
				"x"			"0"
				"y"			"48"
				"width"		"24"
				"height"	"24"
		}
	}
}